//
//  AppDelegate.h
//  SingleViewApp
//
//  Created by Sion Sato on 2013/01/25.
//  Copyright (c) 2013年 Simple Beep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

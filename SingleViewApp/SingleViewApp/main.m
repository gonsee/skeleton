//
//  main.m
//  SingleViewApp
//
//  Created by Sion Sato on 2013/01/25.
//  Copyright (c) 2013年 Simple Beep. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
